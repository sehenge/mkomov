@extends('layouts.articles')
@section('title')
    {{$title}}
@endsection
@section('content')
    @if ( !$posts->count() )
        Нет записей.
    @else
        @php
            $i = 0;
        @endphp
        <div class="container">
            @foreach ($posts as $post)
                @if (!($i % 3))
                    <div class="row">
                        @endif
                        <div class="col-sm col-md-4">
                            <img src="https://cdn.pixabay.com/photo/2020/10/04/16/07/typewriter-5626841_960_720.jpg"/>
                            <h4><a href="{{ url('/articles/' . $post->slug) }}">{{ $post->title }}</a></h4>
                            <article>
                                {!! Str::limit($post->body, $limit = 77, $end = '.... <a href='.url("/articles/" . $post->id).'>Читать далее</a>') !!}
                            </article>
                            <div class="views id-{{$post->id}}">
                                <span><img
                                        src="https://whatsinside.it/wp-content/uploads/2016/09/Views-Icon.png"/></span>
                                <span class="count">{{ $post->views }}</span>
                                <span>
                                    <a onclick="like({{ $post->id }});" style="cursor:pointer;">
                                        <img
                                            src="https://www.iconninja.com/files/995/239/802/heart-health-rating-like-admiration-favorite-love-icon.png"
                                            style="max-width:20px;" ;/>
                                    </a>
                                </span>
                                <span class="count likes">{{ $post->likes }}</span>
                                <span class="count comments">Комментариев: {{ count($post->comments) }}</span>
                            </div>
                        </div>
                        @if (($i++ % 3) == 2)
                    </div>
                    <br/>
                @endif
            @endforeach
        </div><br />
        {!! $posts->render() !!}
    @endif
@endsection
