@extends('layouts.app')
@section('title')
    {{$title}}
@endsection
@section('tags')
    <div class="tags">
        @foreach ($tags as $tag)
            <a href="{{ url('/tag/' . $tag->name) }}"><span class="badge">{{ $tag->name }}</span></a>
        @endforeach
    </div>
@endsection
@section('content')
@if ( !$posts->count() )
    Нет записей.
@else

<div class="">
  @foreach ($posts as $post)
  <div class="list-group">
    <div class="list-group-item">
        <img src="https://cdn.pixabay.com/photo/2020/10/04/16/07/typewriter-5626841_960_720.jpg" />
        <h3><a href="{{ url('/articles/' . $post->slug) }}">{{ $post->title }}</a></h3>
    </div>
    <div class="list-group-item">
        <article>
            {!! Str::limit($post->body, $limit = 1500, $end = '....... <a href='.url("/articles/" . $post->slug).'>Читать далее</a>') !!}
        </article>
        <div class="views id-{{$post->id}}">
            <span><img src="https://whatsinside.it/wp-content/uploads/2016/09/Views-Icon.png" /></span>
            <span class="count">{{ $post->views }}</span>
            <span>
            <a onclick="like({{ $post->id }});" style="cursor:pointer;">
                <img src="https://www.iconninja.com/files/995/239/802/heart-health-rating-like-admiration-favorite-love-icon.png" style="max-width:20px;"; />
            </a>
            </span>
            <span class="count likes">{{ $post->likes }}</span>
            <span class="count comments">Комментариев: {{ count($post->comments) }}</span>
        </div>
    </div>
  </div>
  @endforeach
  {!! $posts->render() !!}
</div>
@endif
@endsection
