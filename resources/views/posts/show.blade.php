@extends('layouts.articles')
@section('title')
    @if($post)
        <img src="https://cdn.pixabay.com/photo/2020/10/04/16/07/typewriter-5626841_960_720.jpg"/>
        <br/><br/>
        {{ $post->title }}
    @else
        Page does not exist
    @endif
@endsection
@section('title-meta')

    @foreach ($post->tag as $singleTag)
        <span class="label label-default label-many"><a
                href="{{ url('tag/' . $singleTag->name) }}">{{ $singleTag->name }}</a></span>
    @endforeach
    <div class="views id-{{$post->id}}">
        <span><img src="https://whatsinside.it/wp-content/uploads/2016/09/Views-Icon.png"/></span>
        <span class="count views" id="views">{{ $post->views }}</span>
        <span>
      <a onclick="like({{ $post->id }});" style="cursor:pointer;">
        <img
            src="https://www.iconninja.com/files/995/239/802/heart-health-rating-like-admiration-favorite-love-icon.png"
            style="max-width:20px;" ;/>
      </a>
    </span>
        <span class="count likes">{{ $post->likes }}</span>
        <span class="count comments">Комментариев: {{ count($comments) }}</span>

        <script>
            setTimeout(view, 5000, {{ $post->id }});
        </script>
    </div>
@endsection
@section('content')
    @if($post)
        <div>
            {!! $post->body !!}
        </div>

        <div>
            <h2>Оставить комментарий</h2>
        </div>
        <div class="panel-body">
            {{ Form::open(array('class'=> 'col-md-6', 'type'=>'POST', 'id'=>'comment', 'action'=>App\Http\Controllers\CommentController::class . '@store')) }}
                <input type="hidden" id="post_id" name="post_id" value="{{ $post->id }}">
                <div class="form-group">
                    {{ Form::text('title', null, ['id' => 'title','placeholder' => 'Тема сообщения', 'maxlength' => 32, 'class'=>'form-control', 'onfocus' => 'this.placeholder = ""', 'onblur' => 'this.placeholder = "Тема сообщения"' ]) }}
                </div>
                <div class="form-group">
                    {{ Form::textarea('body', null, ['id' => 'body', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none', 'class' => 'form-control', 'placeholder' => 'Ваш комментарий', 'onfocus' => 'this.placeholder = ""', 'onblur' => 'this.placeholder = "Ваш комментарий"']) }}
                </div>
                {{Form::button('Отправить', array('type'=> 'submit', 'name' => 'post_comment', 'id' => 'post_comment', 'class' => 'btn btn-dark'))}}
            {{ Form::close() }}
        </div>
        <div>
            @if($comments)
                <ul style="list-style: none; padding: 0">
                    @foreach($comments as $comment)
                        <li class="panel-body">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <h3>{{ $comment->title}}</h3>
                                </div>
                                <div class="list-group-item">
                                    <p class="message">{!! nl2br(e($comment->body)) !!}</p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    @else
        404 error
    @endif
@endsection
