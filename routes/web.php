<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', PostController::class . '@index');
Route::get('articles', PostController::class . '@articles')->name('articles');
Route::get('articles/{slug}', PostController::class . '@show')->name('articles.show');
Route::post('like/{id}', PostController::class . '@like');
Route::get('likes/{id}', PostController::class . '@getLikes');

Route::post('view/{id}', PostController::class . '@viewCount');

Route::post('comment/add', CommentController::class . '@store');
Route::get('tag/{tag}', PostController::class . '@tagSearch');
