<?php

namespace App\Http\Controllers;

use App\Jobs\BlogCommentJob;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|min:1|max:255',
            'body' => 'required|min:1',
        ]);
        $input['post_id'] = $request->input('post_id');
        $input['title'] = $request->input('title');
        $input['body'] = $request->input('body');

        $job = (new BlogCommentJob($input));
        $this->dispatch($job);

        return json_encode(['result' => 'Комментарий отправлен']);
    }
}
