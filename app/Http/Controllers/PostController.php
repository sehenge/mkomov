<?php

namespace App\Http\Controllers;

use \App\Models\Posts;
use \App\Models\Tags;

class PostController extends Controller
{
    public function index()
    {
        $posts = Posts::select()->orderBy('created_at', 'desc')->paginate(6);
        $tags = Tags::get();
        $title = 'Последние записи';

        return view('home')->withPosts($posts)->withTitle($title)->withTags($tags);
    }

    public function articles()
    {
        $posts = Posts::select()->orderBy('created_at', 'desc')->paginate(10);
        $tags = Tags::get();
        $title = 'Каталог статей';

        return view('articles')->withPosts($posts)->withTitle($title)->withTags($tags);
    }

    public function show(string $slug)
    {
        $post = Posts::where('slug', $slug)->first();
        if(!$post) {
            return redirect('/')->withErrors('Запись не найдена');
        }
        $comments = $post->comments;
//        $post->increment('views', 1);

        return view('posts.show')->withPost($post)->withComments($comments->reverse());
    }

    public function viewCount(int $id)
    {
        $post = Posts::where('id', $id)->first();
        $post->increment('views', 1);

        return $post->views;
    }

    public function tagSearch(string $tag)
    {
        $posts = Posts::with('tag')->whereHas('tag', function($q) use ($tag){
            $q->where('name', $tag);
        })->paginate(3);
        $title = 'Результаты поиска по тегу: "' . $tag . '"';

        return view('search')->withPosts($posts)->withTitle($title);
    }

    public function like(int $id)
    {
        $post = Posts::where('id', $id)->first();
        if(!$post) {
            return redirect('/')->withErrors('Запись не найдена');
        }
        $post->increment('likes', 1);

        return $post->likes;
    }

    public function getLikes(int $id)
    {
        $post = Posts::where('id', $id)->first();

        return $post->likes;
    }
}
