<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    use HasFactory;

    protected $guarded = [];

  
    public function comments()
    {
        return $this->hasMany('App\Models\Comments', 'post_id');
    }

    public function tag()
    {
        return $this->belongsToMany('App\Models\Tags', 'post_tag');
    }   
}
