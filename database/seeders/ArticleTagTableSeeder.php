<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ArticleTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 20; $i++) {
            \DB::table('post_tag')->insert(array(
                array(
                    'posts_id' => $i,
                    'tags_id' => $i
                ),
            ));
        }

    }
}
